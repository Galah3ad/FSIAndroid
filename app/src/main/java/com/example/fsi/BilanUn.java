package com.example.fsi;

public class BilanUn {
    public int idBilanUn;
    public float noteEntreprise;
    public float noteDossierUn;
    public float noteOralUn;
    public String dateBilanUn;
    public String remarqueBilanUn;

    public BilanUn(int idBilanUn, float noteEntreprise, float noteDossierUn, float noteOralUn, String dateBilanUn, String remarqueBilanUn) {
        this.idBilanUn = idBilanUn;
        this.noteEntreprise = noteEntreprise;
        this.noteDossierUn = noteDossierUn;
        this.noteOralUn = noteOralUn;
        this.dateBilanUn = dateBilanUn;
        this.remarqueBilanUn = remarqueBilanUn;
    }

    public int getIdBilanUn() {
        return idBilanUn;
    }

    public void setIdBilanUn(int idBilanUn) {
        this.idBilanUn = idBilanUn;
    }

    public float getNoteEntreprise() {
        return noteEntreprise;
    }

    public void setNoteEntreprise(float noteEntreprise) {
        this.noteEntreprise = noteEntreprise;
    }

    public float getNoteDossierUn() {
        return noteDossierUn;
    }

    public void setNoteDossierUn(float noteDossierUn) {
        this.noteDossierUn = noteDossierUn;
    }

    public float getNoteOralUn() {
        return noteOralUn;
    }

    public void setNoteOralUn(float noteOralUn) {
        this.noteOralUn = noteOralUn;
    }

    public String getDateBilanUn() {
        return dateBilanUn;
    }

    public void setDateBilanUn(String dateBilanUn) {
        this.dateBilanUn = dateBilanUn;
    }

    public String getRemarqueBilanUn() {
        return remarqueBilanUn;
    }

    public void setRemarqueBilanUn(String remarqueBilanUn) {
        this.remarqueBilanUn = remarqueBilanUn;
    }
}
