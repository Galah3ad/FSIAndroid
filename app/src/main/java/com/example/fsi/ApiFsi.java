package com.example.fsi;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiFsi {

    String BASE_URL = "https://olen-ort.fr/p2024/Ma2l/API/";

    @GET("etudiants/{id}")
    Call<Etudiant> getEtudiantById(@Path("id") int idEtudiant);

}
