package com.example.fsi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;

public class HomeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button btnConsultNotes = findViewById(R.id.btnConsultNotes);
        Button btnConsultInfo = findViewById(R.id.btnConsultInfo);

        btnConsultNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ConsultNotesActivity.class);
                startActivity(intent);
            }
        });

        btnConsultInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, StudentInfoActivity.class);
                startActivity(intent);
            }

        });



    }
}
