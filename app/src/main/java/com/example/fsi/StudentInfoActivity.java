package com.example.fsi;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class StudentInfoActivity extends AppCompatActivity {
    private TextView tvNomEtudiant, tvPrenomEtudiant, tvTelephoneEtudiant, tvEmailEtudiant;
    private TextView tvEntrepriseEtudiant, tvTuteurEntreprise, tvTuteurEcole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_infos);

        initializeViews();

        displayStudentInfo();


        Button buttonRetourInfo = findViewById(R.id.buttonRetourInfo);
        buttonRetourInfo.setOnClickListener(v -> finish());
    }

    private void initializeViews() {
        tvNomEtudiant = findViewById(R.id.tvNomEtudiant);
        tvPrenomEtudiant = findViewById(R.id.tvPrenomEtudiant);
        tvTelephoneEtudiant = findViewById(R.id.tvTelephoneEtudiant);
        tvEmailEtudiant = findViewById(R.id.tvEmailEtudiant);
        tvEntrepriseEtudiant = findViewById(R.id.tvEntrepriseEtudiant);
        tvTuteurEntreprise = findViewById(R.id.tvTuteurEntreprise);
        tvTuteurEcole = findViewById(R.id.tvTuteurEcole);
    }

    private void displayStudentInfo() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(this);
        try (SQLiteDatabase db = dbHelper.getReadableDatabase();
             Cursor cursor = db.query("Etudiant", null, null, null, null, null, null)) {
            if (cursor.moveToFirst()) {
                tvNomEtudiant.setText(cursor.getString(cursor.getColumnIndexOrThrow("nomEtudiant")));
                tvPrenomEtudiant.setText(cursor.getString(cursor.getColumnIndexOrThrow("prenomEtudiant")));
                tvTelephoneEtudiant.setText(cursor.getString(cursor.getColumnIndexOrThrow("telephoneEtudiant")));
                tvEmailEtudiant.setText(cursor.getString(cursor.getColumnIndexOrThrow("mailEtudiant")));
                tvEntrepriseEtudiant.setText(cursor.getString(cursor.getColumnIndexOrThrow("entrepriseEtudiant")));
                tvTuteurEntreprise.setText(cursor.getString(cursor.getColumnIndexOrThrow("tuteurEntrepriseEtudiant")));
                tvTuteurEcole.setText(cursor.getString(cursor.getColumnIndexOrThrow("tuteurEcoleEtudiant")));
            } else {

            }
        }
    }
}
