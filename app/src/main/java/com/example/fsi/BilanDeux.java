package com.example.fsi;

public class BilanDeux {
    private int idBilanDeux;
    private float noteOralDeux;
    private float noteDossierDeux;
    private String dateBilanDeux;
    private String remarqueBilanDeux;
    private String sujetMemoire;

    public BilanDeux(int idBilanDeux, float noteOralDeux, float noteDossierDeux, String dateBilanDeux, String remarqueBilanDeux, String sujetMemoire) {
        this.idBilanDeux = idBilanDeux;
        this.noteOralDeux = noteOralDeux;
        this.noteDossierDeux = noteDossierDeux;
        this.dateBilanDeux = dateBilanDeux;
        this.remarqueBilanDeux = remarqueBilanDeux;
        this.sujetMemoire = sujetMemoire;
    }

    public int getIdBilanDeux() {
        return idBilanDeux;
    }

    public void setIdBilanDeux(int idBilanDeux) {
        this.idBilanDeux = idBilanDeux;
    }

    public float getNoteOralDeux() {
        return noteOralDeux;
    }

    public void setNoteOralDeux(float noteOralDeux) {
        this.noteOralDeux = noteOralDeux;
    }

    public float getNoteDossierDeux() {
        return noteDossierDeux;
    }

    public void setNoteDossierDeux(float noteDossierDeux) {
        this.noteDossierDeux = noteDossierDeux;
    }

    public String getDateBilanDeux() {
        return dateBilanDeux;
    }

    public void setDateBilanDeux(String dateBilanDeux) {
        this.dateBilanDeux = dateBilanDeux;
    }

    public String getRemarqueBilanDeux() {
        return remarqueBilanDeux;
    }

    public void setRemarqueBilanDeux(String remarqueBilanDeux) {
        this.remarqueBilanDeux = remarqueBilanDeux;
    }

    public String getSujetMemoire() {
        return sujetMemoire;
    }

    public void setSujetMemoire(String sujetMemoire) {
        this.sujetMemoire = sujetMemoire;
    }
}
