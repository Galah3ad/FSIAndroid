package com.example.fsi;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class ConsultNotesActivity extends AppCompatActivity {
    private TextView tvNoteEntrepriseBilanUn, tvNoteDossierUn, tvNoteOralUn, tvDateBilanUn, tvRemarqueBilanUn;
    private TextView tvNoteOralDeux, tvNoteDossierDeux, tvDateBilanDeux, tvRemarqueBilanDeux, tvSujetMemoire;
    private Button buttonRetour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_notes);

        initializeViews();
        displayBilanUnData();
        displayBilanDeuxData();

        buttonRetour.setOnClickListener(v -> finish());
    }

    private void initializeViews() {
        tvNoteEntrepriseBilanUn = findViewById(R.id.tvNoteEntrepriseBilanUn);
        tvNoteDossierUn = findViewById(R.id.tvNoteDossierUn);
        tvNoteOralUn = findViewById(R.id.tvNoteOralUn);
        tvDateBilanUn = findViewById(R.id.tvDateBilanUn);
        tvRemarqueBilanUn = findViewById(R.id.tvRemarqueBilanUn);

        tvNoteOralDeux = findViewById(R.id.tvNoteOralDeux);
        tvNoteDossierDeux = findViewById(R.id.tvNoteDossierDeux);
        tvDateBilanDeux = findViewById(R.id.tvDateBilanDeux);
        tvRemarqueBilanDeux = findViewById(R.id.tvRemarqueBilanDeux);
        tvSujetMemoire = findViewById(R.id.tvSujetMemoire);

        buttonRetour = findViewById(R.id.buttonRetour);
    }

    private void displayBilanUnData() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(this);
        try (SQLiteDatabase db = dbHelper.getReadableDatabase();
             Cursor cursor = db.query("BilanUn", null, null, null, null, null, null)) {
            if (cursor.moveToFirst()) {
                tvNoteEntrepriseBilanUn.setText(cursor.getString(cursor.getColumnIndexOrThrow("noteEntreprise")));
                tvNoteDossierUn.setText(cursor.getString(cursor.getColumnIndexOrThrow("noteDossierUn")));
                tvNoteOralUn.setText(cursor.getString(cursor.getColumnIndexOrThrow("noteOralUn")));
                tvDateBilanUn.setText(cursor.getString(cursor.getColumnIndexOrThrow("dateBilanUn")));
                tvRemarqueBilanUn.setText(cursor.getString(cursor.getColumnIndexOrThrow("remarqueBilanUn")));
            }
        }
    }

    private void displayBilanDeuxData() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(this);
        try (SQLiteDatabase db = dbHelper.getReadableDatabase();
             Cursor cursor = db.query("BilanDeux", null, null, null, null, null, null)) {
            if (cursor.moveToFirst()) {
                tvNoteOralDeux.setText(cursor.getString(cursor.getColumnIndexOrThrow("noteOralDeux")));
                tvNoteDossierDeux.setText(cursor.getString(cursor.getColumnIndexOrThrow("noteDossierDeux")));
                tvDateBilanDeux.setText(cursor.getString(cursor.getColumnIndexOrThrow("dateBilanDeux")));
                tvRemarqueBilanDeux.setText(cursor.getString(cursor.getColumnIndexOrThrow("remarqueBilanDeux")));
                tvSujetMemoire.setText(cursor.getString(cursor.getColumnIndexOrThrow("sujetMemoire")));
            }
        }
    }
}
