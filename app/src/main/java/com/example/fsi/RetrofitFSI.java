package com.example.fsi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFSI {
    private static RetrofitFSI instance = null;
    private ApiFsi myApi;
    private RetrofitFSI() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiFsi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        myApi = retrofit.create(ApiFsi.class);
    }

    public static synchronized RetrofitFSI getInstance() {
        if(instance == null) {
            instance = new RetrofitFSI();
        }
        return instance;
    }

    public ApiFsi getMyApi()
    {
        return myApi;
    }
}
