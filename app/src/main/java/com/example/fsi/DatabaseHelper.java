package com.example.fsi;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "fsi_database";
    private static final int DATABASE_VERSION = 2;

    private static DatabaseHelper instanmamce;

    private static final String CREATE_TABLE_ETUDIANT = "CREATE TABLE Etudiant (" +
            "idEtudiant INTEGER PRIMARY KEY AUTOINCREMENT," +
            "nomEtudiant TEXT," +
            "prenomEtudiant TEXT," +
            "telephoneEtudiant TEXT," +
            "mailEtudiant TEXT," +
            "entrepriseEtudiant TEXT," +
            "tuteurEntrepriseEtudiant TEXT," +
            "tuteurEcoleEtudiant TEXT," +
            "loginEtudiant TEXT," +
            "mdpEtudiant TEXT" +
            ")";

    private static final String CREATE_TABLE_BILAN_UN = "CREATE TABLE BilanUn (" +
            "idBilanUn INTEGER PRIMARY KEY AUTOINCREMENT," +
            "noteEntreprise REAL," +
            "noteDossierUn REAL," +
            "noteOralUn REAL," +
            "dateBilanUn TEXT," +
            "remarqueBilanUn TEXT" +
            ")";

    private static final String CREATE_TABLE_BILAN_DEUX = "CREATE TABLE BilanDeux (" +
            "idBilanDeux INTEGER PRIMARY KEY AUTOINCREMENT," +
            "noteOralDeux REAL," +
            "noteDossierDeux REAL," +
            "dateBilanDeux TEXT," +
            "remarqueBilanDeux TEXT," +
            "sujetMemoire TEXT" +
            ")";

    public static synchronized DatabaseHelper getInstance(Context context) {
        DatabaseHelper instance = null;
        instance = new DatabaseHelper(context.getApplicationContext());
        return instance;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ETUDIANT);
        db.execSQL(CREATE_TABLE_BILAN_UN);
        db.execSQL(CREATE_TABLE_BILAN_DEUX);
        insertInitialData(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Etudiant");
        db.execSQL("DROP TABLE IF EXISTS BilanUn");
        db.execSQL("DROP TABLE IF EXISTS BilanDeux");
        onCreate(db);
    }

    private void insertInitialData(SQLiteDatabase db) {
        insertEtudiant(db, "NIDAM", "Mamoune", "0669996345", "nidam.mamoune@gmail.com", "Teamwork", "Mr BARTHEBAUD", "Mme GOUDET", "mamoune.nidam", "admin123");
        insertBilanUn(db, 15.5f, 14f, 16f, "2023-03-20", "Très bon travail");
        insertBilanDeux(db, 17f, 18f, "2023-06-15", "Excellent", "Développement Android avec SQLite");
    }

    private void insertEtudiant(SQLiteDatabase db, String nom, String prenom, String tel, String email, String entreprise, String tuteurEntreprise, String tuteurEcole, String login, String mdp) {
        ContentValues cv = new ContentValues();
        cv.put("nomEtudiant", nom);
        cv.put("prenomEtudiant", prenom);
        cv.put("telephoneEtudiant", tel);
        cv.put("mailEtudiant", email);
        cv.put("entrepriseEtudiant", entreprise);
        cv.put("tuteurEntrepriseEtudiant", tuteurEntreprise);
        cv.put("tuteurEcoleEtudiant", tuteurEcole);
        cv.put("loginEtudiant", login);
        cv.put("mdpEtudiant", mdp);
        db.insert("Etudiant", null, cv);
    }

    private void insertBilanUn(SQLiteDatabase db, float noteEntreprise, float noteDossier, float noteOral, String date, String remarque) {
        ContentValues cv = new ContentValues();
        cv.put("noteEntreprise", noteEntreprise);
        cv.put("noteDossierUn", noteDossier);
        cv.put("noteOralUn", noteOral);
        cv.put("dateBilanUn", date);
        cv.put("remarqueBilanUn", remarque);
        db.insertOrThrow("BilanUn", null, cv);
    }

    private void insertBilanDeux(SQLiteDatabase db, float noteOral, float noteDossier, String date, String remarque, String sujetMemoire) {
        ContentValues cv = new ContentValues();
        cv.put("noteOralDeux", noteOral);
        cv.put("noteDossierDeux", noteDossier);
        cv.put("dateBilanDeux", date);
        cv.put("remarqueBilanDeux", remarque);
        cv.put("sujetMemoire", sujetMemoire);
        db.insertOrThrow("BilanDeux", null, cv);
    }
}

