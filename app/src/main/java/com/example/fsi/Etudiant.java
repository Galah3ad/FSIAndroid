package com.example.fsi;

public class Etudiant {
    public int idEtudiant;
    public String nomEtudiant;
    public String prenomEtudiant;
    public String telephoneEtudiant;
    public String mailEtudiant;
    public String entrepriseEtudiant;
    public String tuteurEntrepriseEtudiant;
    public String tuteurEcoleEtudiant;
    public String loginEtudiant;
    public String mdpEtudiant;

    public Etudiant(int idEtudiant, String nomEtudiant, String prenomEtudiant,
                    String telephoneEtudiant, String mailEtudiant,
                    String entrepriseEtudiant, String tuteurEntrepriseEtudiant,
                    String tuteurEcoleEtudiant, String loginEtudiant,
                    String mdpEtudiant) {
        this.idEtudiant = idEtudiant;
        this.nomEtudiant = nomEtudiant;
        this.prenomEtudiant = prenomEtudiant;
        this.telephoneEtudiant = telephoneEtudiant;
        this.mailEtudiant = mailEtudiant;
        this.entrepriseEtudiant = entrepriseEtudiant;
        this.tuteurEntrepriseEtudiant = tuteurEntrepriseEtudiant;
        this.tuteurEcoleEtudiant = tuteurEcoleEtudiant;
        this.loginEtudiant = loginEtudiant;
        this.mdpEtudiant = mdpEtudiant;
    }

    public int getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(int idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public String getNomEtudiant() {
        return nomEtudiant;
    }

    public void setNomEtudiant(String nomEtudiant) {
        this.nomEtudiant = nomEtudiant;
    }

    public String getPrenomEtudiant() {
        return prenomEtudiant;
    }

    public void setPrenomEtudiant(String prenomEtudiant) {
        this.prenomEtudiant = prenomEtudiant;
    }

    public String getTelephoneEtudiant() {
        return telephoneEtudiant;
    }

    public void setTelephoneEtudiant(String telephoneEtudiant) {
        this.telephoneEtudiant = telephoneEtudiant;
    }

    public String getMailEtudiant() {
        return mailEtudiant;
    }

    public void setMailEtudiant(String mailEtudiant) {
        this.mailEtudiant = mailEtudiant;
    }

    public String getEntrepriseEtudiant() {
        return entrepriseEtudiant;
    }

    public void setEntrepriseEtudiant(String entrepriseEtudiant) {
        this.entrepriseEtudiant = entrepriseEtudiant;
    }

    public String getTuteurEntrepriseEtudiant() {
        return tuteurEntrepriseEtudiant;
    }

    public void setTuteurEntrepriseEtudiant(String tuteurEntrepriseEtudiant) {
        this.tuteurEntrepriseEtudiant = tuteurEntrepriseEtudiant;
    }

    public String getTuteurEcoleEtudiant() {
        return tuteurEcoleEtudiant;
    }

    public void setTuteurEcoleEtudiant(String tuteurEcoleEtudiant) {
        this.tuteurEcoleEtudiant = tuteurEcoleEtudiant;
    }

    public String getLoginEtudiant() {
        return loginEtudiant;
    }

    public void setLoginEtudiant(String loginEtudiant) {
        this.loginEtudiant = loginEtudiant;
    }

    public String getMdpEtudiant() {
        return mdpEtudiant;
    }

    public void setMdpEtudiant(String mdpEtudiant) {
        this.mdpEtudiant = mdpEtudiant;
    }
}
